import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { firstValueFrom, forkJoin } from 'rxjs';
import { RequestService } from 'src/app/service/request.service';

@Component({
  selector: 'app-carrusel',
  templateUrl: './carrusel.component.html',
  styleUrls: ['./carrusel.component.css']
})
export class CarruselComponent {
  currentUser: any;
  lastestUser: any;
  @Input() item: any;
  data: any = "";
  userQty: number | undefined;
  modalinfo: any;
  loadingExtraInfo = false;
  @Input('post') post?: number = 0;
  @Input('laps') laps?: number | undefined = 0;
 
  requestService: any;
  showArrows = true;
  modalOpen = false;
  id: any = 0;
  idp: any = 0;

  breakpointsUser = {
    550: { perView: 1 },
    991: { perView: 2 },
    1000: { perView: 3 },
    1200: { perView: 3 },
  };
  currentUserIndex = 0;
  @Output() idPost: EventEmitter<any> = new EventEmitter();

  constructor(private service: RequestService) { }

  ngOnInit(): void {
    if (this.post !== undefined) {
        this.loadExtraInfo();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['post'] && !changes['post'].firstChange) {
      this.loadExtraInfo();
    }
  }

  async loadExtraInfo() {
    this.loadingExtraInfo = true;
    try {
      const resp = await firstValueFrom(
        forkJoin([
          this.service.get(this.laps ?  `albums/${this.post}/photos` : this.post ? `users/${this.post}/albums` : `users/`),
        ])
      );
      this.lastestUser = resp[0];
      console.log(this.lastestUser)
      this.lastestUser.forEach((element: { storyIndex: any; }, index: any) => { element.storyIndex = index });
      this.userQty = this.lastestUser.length;
      this.loadingExtraInfo = false;
    } catch (err) {
      console.error(err);
    }
  }

  opencarrusel(id: any) {
    this.idPost.emit(id);
  }

  nextTip() {
    if (this.currentUserIndex < this.lastestUser.length - 1) {
      this.currentUserIndex++;
    }
  }

  lastTip() {
    if (this.currentUserIndex > 0) {
      this.currentUserIndex--;
    }
  }


}