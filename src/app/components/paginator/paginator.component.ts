import { firstValueFrom } from 'rxjs';
import {
  Component,
  ContentChild,
  TemplateRef,
  Input,
  Output,
  EventEmitter,
  ElementRef
} from '@angular/core';

import { RequestService } from 'src/app/service/request.service';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent {
  lastcomment: any;
  
  @ContentChild(TemplateRef) templateRef!: TemplateRef<any>;

  @Input()
  showPagination = true;

  @Input()
  endpoint = '';

  @Input()
  classContent = 'row h-100';

  @Input()
  queryParams = {};

  @Input()
  pageSize = 2;

  @Input()
  area:any;

  @Input()
  canScrollTo = true;

  @Output()
  onLoaded: EventEmitter<any> = new EventEmitter<any>();

  page = 2;
  totalPages = 2;

  info: any;

  isLoading = false;
  id: any;

  modalinfo: any;
  constructor(private request: RequestService  , private el: ElementRef) {
    const iduser = localStorage.getItem('iduser');
    this.id = iduser
   }

   ngOnInit(): void {
   this.getInfo();
   }  
  
  async getInfo() {
    this.isLoading = true;
    try {
      this.info = await firstValueFrom(
        this.request.get(this.endpoint, {
          userId:this.id ,
          page: this.page,
          page_size: this.pageSize,
        })
      );
      console.log(this.info);
      this.totalPages = this.info?.length
      ? Math.ceil(this.info.length / this.pageSize)
      : 0;
    } catch (err) {
      this.totalPages = 0;
    }
    this.onLoaded.emit();
    this.isLoading = false;
    if (this.canScrollTo) {
      this.el.nativeElement.scrollIntoView();
    }
  } 

  reset() {
    this.info = null;
    this.page = 1;
    this.totalPages = 0;
  }

  async previous() {
    if (!this.info) {
      return;
    }
    if (this.page - 1 < 1) {
      return;
    }
    this.page -= 1;
    await this.getInfo();
  }

  async next() {
    if (!this.info) {
      return;
    }
    this.page += 1;
    await this.getInfo();
  }

  get pagesItems(): Array<number> {
    if (this.info?.length) {
      const maxItems = this.totalPages < 5 ? this.totalPages : 5;
      return Array(maxItems)
        .fill('')
        .map((_, item) => {
          let increment = 0;
          if (this.page > 3) {
            increment = this.page - 3;
          }
          if (this.page >= this.totalPages - 2) {
            increment = this.totalPages - maxItems;
          }
          return item + 1 + increment;
        });
    }
    return [];
  }

  async goTo(page: number) {
    if (this.page == page) {
      return;
    }
    this.page = page;
    await this.getInfo();
  }
}