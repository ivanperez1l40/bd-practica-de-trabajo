import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
myForm: FormGroup = this.fb.group({
  name:['',[Validators.required]],
  userName:['',[Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  password: ['', [Validators.required, Validators.minLength(6)]]
});

constructor(private fb: FormBuilder,private service:AuthService){}

register(){
  this.service.post(this.myForm.value).subscribe((res:any)=> {
  });
}

}
