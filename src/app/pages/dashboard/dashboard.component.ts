import { Component, ElementRef, ViewChild } from '@angular/core';
import { RequestService } from 'src/app/service/request.service';
declare const bootstrap: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
@ViewChild('modalInfo',{static:true}) modalInfo?:ElementRef<HTMLDivElement>;
post?:number;
showSecondCarousel = false;
showThirdCarousel = false;
laps: number = 0;
modalinfo: any;
comments: any;

constructor(private service:RequestService){}

ngOnInit(): void { 
  console.log(this.modalInfo)
  this.modalinfo = new bootstrap.Modal(this.modalInfo?.nativeElement); 
  }  

  openModal(value:any){
    try {
    this.service.get(`posts/${value}/comments`).subscribe((res:any) => {
     this.comments = res;
    })
   this.modalinfo.show();
     } catch(err) {
      console.log(err)
     }  
  }

showPost(value:any){
  this.post = value;
  this.laps = this.laps + 1;
  this.showSecondCarousel = true;
  console.log(this.laps)
  if(this.laps > 1){
    this.showThirdCarousel = true;
  }
} 

}
