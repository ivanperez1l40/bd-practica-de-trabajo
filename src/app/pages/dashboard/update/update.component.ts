import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/interfaces/User';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent {
myForm : FormGroup = this.fb.group({
  name:['',[Validators.required]],
  userName:['',[Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  password: ['', [Validators.required, Validators.minLength(6)]]
});
id:any;
constructor(private fb:FormBuilder,private service:AuthService){}

ngOnInit(): void {
  this.id =localStorage.getItem('iduser');
  console.log(this.id)
 
  this.service.get("users","id",this.id).subscribe((res:User | undefined) => {
    console.log(res)
   this.myForm.patchValue(res!)
   this.id = parseInt(this.id);
  })
  } 

update(){
  this.service.update(this.myForm.value,this.id).subscribe((res:any) =>{
    console.log(res)
   })
}
}
