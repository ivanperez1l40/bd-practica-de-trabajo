import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
   path:'login',
   loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule)
  },
  {
    path:'register',
    loadChildren: () => import('./pages/auth/register/register.module').then(m => m.RegisterModule)
  },
{
  path:'',
  component:LayoutComponent,
  children:[
    { 
      path:'dashboard',
      loadChildren:() => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
     path:'update',
     loadChildren:() => import('./pages/dashboard/update/update.module').then(m => m.UpdateModule)
    }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
