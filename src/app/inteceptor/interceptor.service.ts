import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  constructor() { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let url = '';
      if (req.url.startsWith('http') || req.url.startsWith('/assets') ) {
      url = req.url;
    } else {
      url = environment.baseUrl;
      url = `${url}${req.url}`;
      console.log(url);
    }

    const request = req.clone({
      url
    });

    return next.handle(request);
  }
}
