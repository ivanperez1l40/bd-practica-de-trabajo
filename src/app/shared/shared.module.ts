import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxGlideModule } from 'ngx-glide';

import { CarruselComponent } from '../components/carrusel/carrusel.component';
import { PaginatorComponent } from '../components/paginator/paginator.component';
import { LottieModule } from 'ngx-lottie';

export function playerFactory(): any {
  return import('lottie-web');
}

@NgModule({
  declarations: [
    CarruselComponent,
    PaginatorComponent
  ],
  imports: [
    CommonModule,
    NgxGlideModule,
    LottieModule.forRoot({
      player: playerFactory
    }),
  ],
  exports:[
    CarruselComponent,
    PaginatorComponent
  ]
})
export class SharedModule { }