import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http: HttpClient) { }

  get(url: string, params = {}, headers = {}): Observable<any> {
    return this.http.get(url, {
      
      params: {
        ...params
      },
      headers: {
        ...headers
      }
    });
    
  }
}
