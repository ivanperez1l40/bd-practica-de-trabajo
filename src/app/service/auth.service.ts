import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../interfaces/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  get(key : string,searchWord:string , params: any): Observable<User | undefined> {
    console.log(params)
    const tablaUser = JSON.parse(localStorage.getItem(key) || "[]");
    const user = tablaUser.find((res: any) => {
      if (typeof params !== 'object') {
        console.log('entre')
        params = JSON.parse(params);
        return res[searchWord] === params;
      } else{//este es el de login
        return res[searchWord] == params[searchWord]
      }
    });
    return of(user);
  }

  post(User:User){
  
    var idUsers =  JSON.parse(localStorage.getItem("idusers") || "[]");
    if(idUsers == null){
      idUsers = 1
    }
    var tableUsers = JSON.parse(localStorage.getItem("users") || "[]");
    if(!Array.isArray(tableUsers)){
      tableUsers = [];
    }
    var objUsers = {
      id : (idUsers > 1) ? idUsers : (tableUsers.length + 1),
      name: User.name,
      userName: User.userName,
      email:User.email,
      password: User.password
    }
     tableUsers.push(objUsers);
     localStorage.setItem("users", JSON.stringify(tableUsers));
      console.log(tableUsers)
     return of(tableUsers)
   }

   update(user: User, newUser: User):Observable<User | undefined> {
    var tablaUser = JSON.parse(localStorage.getItem("users") || "[]");
    let index = tablaUser.findIndex((res: { id: any; }) => res.id === newUser);
    const id:any = { id: newUser };
    const updatedUsers = { ...newUser, id: newUser.id };
    updatedUsers.id = id.id;
    updatedUsers.name = user.name;
    updatedUsers.userName = user.userName;
    updatedUsers.email = user.email;
    updatedUsers.password = user.password;
   
     if(index == -1){
      index = 0
     }
     tablaUser[index] = updatedUsers;
     localStorage.setItem('users', JSON.stringify(tablaUser));
    return of(updatedUsers) 
   }
}
